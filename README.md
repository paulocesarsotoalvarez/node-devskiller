**README APP**   
---  

The task on this test is not working as expectd, the download image from docker randomly stucked.

  build a docker image for the sample application with `nodejs/app:latest` tag  
  
  - Command  
   ```sh  
   $ docker build . -t nodejs/app:latest
   ```

 My solution:   
 - Create a repo on my account.  
 - Upload & download on my local machine.  
 - Build the container.  (FAST) 
 - Upload the image to my Docker Hub (FAST)  
 - Download image on Machine Test (Again Stucked)

 Solution two:  
 - Save Image genereted previously.  
 - Zip Image.  
 - Copy image using scp to test machine.  
 - UnZip image on Test machine.  
 - Load image on Test machine.   
 - Execute second instruction indicated on test.
    - un the image in the background - make sure to expose port 8080 to the world!  
 
    ```sh  
    docker run -d -p 8080:8080 nodejs/app:latest  
    ```  