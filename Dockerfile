FROM node:carbon
WORKDIR /srv/app
COPY . .
RUN npm install --only=production
CMD [ "npm", "start" ]
EXPOSE 8080
